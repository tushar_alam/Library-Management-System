﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="IssueBook.aspx.cs" Inherits="LibraryManagementSystem.IssueBook" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container well well-sm " style="width: auto; height: auto">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel-heading bg-primary" style="text-align: center; width: auto; color: #fff">Book Issue Form</div>


                <div class="form-group" style="margin-top: 2%">
                    <asp:Label CssClass="col-xs-2" ID="Label1" runat="server"><b>Select Publication</b></asp:Label>
                    <div class="col-xs-4">
                        <asp:DropDownList AutoPostBack="true" OnSelectedIndexChanged="ddlPublication_SelectedIndexChanged" class="form-control" ID="ddlPublication" runat="server" DataSourceID="SqlDataSource1" DataTextField="PublicationName" DataValueField="PublicationName"></asp:DropDownList>

                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:LibraryConnectionString2 %>" SelectCommand="SELECT [PublicationName] FROM [PublicationTable]"></asp:SqlDataSource>
                    </div>
                </div>

                <div class="form-group" style="margin-top: 2%">
                    <asp:Label CssClass="col-xs-2" ID="Label2" runat="server"><b>Select Book</b></asp:Label>
                    <div class="col-xs-4">
                        <asp:DropDownList  class="form-control" ID="ddlBook" runat="server"></asp:DropDownList>

                    </div>
                </div>

                <div class="form-group" style="margin-top: 5%">
                    <div class="col-xs-2">
                        <asp:Button CssClass="btn btn-success" ID="btnSelectBook" Text="Select" runat="server" OnClick="btnSelectBook_Click" />
                    </div>
                    <div class="col-xs-4">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </div>
                </div>

            </div>
        </div>



        <div class="row">
            <div class="col-xs-12">
       
                       <asp:Repeater ID="rptrBookData" runat="server">
                    <ItemTemplate>
                        <asp:Panel Style="border:2px outset cadetblue; margin: 5px; float: left; padding: 5px; height: 250px; width: 350px; color: #fff; background-color:cornflowerblue" ID="pnlBookData" runat="server">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblBookName" Text='<%#Eval("BookName") %>' runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Image ID="Image1" ImageUrl='<%#Eval("Image")%>' Style="width: 175px; height: 210px" runat="server" />
                                    </td>
                                    <td>
                                        <table style="margin-left: 5px">
                                            <tr>
                                                <td><b>Autor</b></td>
                                                <td>
                                                    <asp:Label ID="Label1" runat="server" Text='<%#Eval("Author") %>'></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><b>Publication: </b></td>
                                                <td>
                                                    <asp:Label ID="Label2" runat="server" Text='<%#Eval("Publication") %>'></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>Branch: </b>

                                                </td>
                                                <td>
                                                    <asp:Label ID="Label3" runat="server" Text='<%#Eval("Branch") %>'></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>Price: </b>

                                                </td>
                                                <td>
                                                    <asp:Label ID="Label4" runat="server" Text='<%#Eval("Price") %>'></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>Quantity: </b>
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label5" runat="server" Text='<%#Eval("Quantity") %>'></asp:Label>
                                                </td>
                                            </tr>
                                            <%-- <tr>
                                                <td>
                                                    <b>Ram: </b>

                                                </td>
                                                <td>
                                                    <asp:Label ID="Label6" runat="server" Text='<%#Eval("ram") %>'></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>Battery: </b>

                                                </td>
                                                <td>
                                                    <asp:Label ID="Label7" runat="server" Text='<%#Eval("Battery") %>'></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>Price: </b>

                                                </td>
                                                <td>
                                                    <asp:Label ID="Label8" runat="server" Text='<%#Eval("price") %>'>Tk</asp:Label>
                                                </td>
                                            </tr>--%>
                                        </table>
                                    </td>
                                </tr>
                            </table>

                        </asp:Panel>

                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="form-group" style="margin-top: 2%">
                    <asp:Label CssClass="col-xs-2" ID="Label6" runat="server"><b>Select Branch</b></asp:Label>

                    <div class="col-xs-4">
                        <asp:DropDownList AutoPostBack="true" OnSelectedIndexChanged="ddlBranch_SelectedIndexChanged"  class="form-control" ID="ddlBranch" runat="server" DataSourceID="SqlDataSource3" DataTextField="BranchName" DataValueField="BranchName"></asp:DropDownList>
                        <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:LibraryConnectionString2 %>" SelectCommand="SELECT [BranchName] FROM [TblBranch]"></asp:SqlDataSource>

                    </div>
                </div>

                <div class="form-group" style="margin-top: 2%">
                    <asp:Label CssClass="col-xs-2" ID="Label7" runat="server"><b>Select Student</b></asp:Label>
                    <div class="col-xs-4">
                        <asp:DropDownList class="form-control" ID="ddlStudent" runat="server"></asp:DropDownList>
                      
                         </div>
                </div>
                <br />

                <div class="form-group" style="margin-top: 5%">
                    <asp:Label CssClass="col-xs-2" runat="server"><b>Days</b></asp:Label>
                    <div class="col-xs-4">
                        <asp:TextBox CssClass="form-control" ID="txtDays" runat="server"></asp:TextBox>

                    </div>
                </div>

            </div>

        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="form-group" style="margin-top: 2%">
                    <div class=" col-xs-offset-2 col-xs-2">
                        <asp:Button ID="btnIssueBook" CssClass="btn btn-success" Text="Book Issue" runat="server" OnClick="btnIssueBook_Click" />
                    </div>
                    <div class="col-xs-4">
                        <asp:Label ID="lblMessage2" runat="server"></asp:Label>
                        <asp:Calendar ID="Calendar1" runat="server"></asp:Calendar>
                    </div>
                </div>
            </div>
        </div>


    </div>

</asp:Content>
