﻿using LibraryManagementSystem.Dao;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace LibraryManagementSystem.Dal
{
    public class DalLogin
    {
        bool status;
        private static readonly string CS = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;


        internal bool FindMailPass(DaoLogin oDaoLogin)
        {
           
                SqlConnection con = new SqlConnection(CS);

                string query = "select *from Login where Email=@Email and Password=@Password";

                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    con.Open();
                    cmd.Parameters.AddWithValue("@Email", oDaoLogin.Email);
                    cmd.Parameters.AddWithValue("@Password", oDaoLogin.Password);

                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.HasRows)
                    {
                        status = true;
                    }
                }
                con.Close();
    
            return status;

        }
    }
}