﻿using LibraryManagementSystem.Bll;
using LibraryManagementSystem.Dao;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace LibraryManagementSystem.Dal
{
    public class DalPublication
    {
        private static readonly string CS = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;


        bool status = false;
        DataTable dt = new DataTable();


        internal bool AddPublicationName(DaoPublication oDaoPub)
        {
            SqlConnection con = new SqlConnection(CS);

            string query = "insert into PublicationTable (PublicationName) values (@PublicationName)";
            using (SqlCommand cmd = new SqlCommand(query,con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@PublicationName", oDaoPub.PublicationName);

                
                int AffectedRow = cmd.ExecuteNonQuery();

                if(AffectedRow>=1)
                {
                    status = true;
                }

            }
            con.Close();
                return status;
        }

        internal DataTable ShowPubData()
        {
            SqlConnection con = new SqlConnection(CS);

            string query = "Select * from PublicationTable ";

            using (SqlCommand cmd = new SqlCommand(query, con))
            {
                con.Open();

                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(dt);
            }
            con.Close();


            return dt;
        }

    }
}