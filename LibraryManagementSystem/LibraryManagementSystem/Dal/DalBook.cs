﻿using LibraryManagementSystem.Dao;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace LibraryManagementSystem.Dal
{
    public class DalBook
    {
        bool status = false;
        bool QuantityUpdate = false;
        DataTable dt = new DataTable();

        private static readonly string CS = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;

        internal bool AddBook(DaoBook oDaoBook)
        {
            SqlConnection con = new SqlConnection(CS);
            string query = "insert into TblBook (BookName,Details,Author,Publication,Branch,Price,Quantity,Image) values (@BookName,@Details,@Author,@Publication,@Branch,@Price,@Quantity,@Image)";

            using (SqlCommand cmd = new SqlCommand(query, con))
            {
                con.Open();

                cmd.Parameters.AddWithValue("@BookName", oDaoBook.BookName);
                cmd.Parameters.AddWithValue("@Details", oDaoBook.Details);
                cmd.Parameters.AddWithValue("@Author", oDaoBook.Author);
                cmd.Parameters.AddWithValue("@Publication", oDaoBook.Publication);
                cmd.Parameters.AddWithValue("@Branch", oDaoBook.Branch);
                cmd.Parameters.AddWithValue("@Price", oDaoBook.Price);
                cmd.Parameters.AddWithValue("@Quantity", oDaoBook.Quantity);
                cmd.Parameters.AddWithValue("@Image", oDaoBook.Image);

                int AffectedRow = cmd.ExecuteNonQuery();

                if (AffectedRow >= 1)
                {
                    status = true;
                }
            }
            con.Close();

            return status;
        }

        internal DataTable ViewBook(DaoBook oDaoBook)
        {

            SqlConnection con = new SqlConnection(CS);

            string query = "Select *from TblBook where Branch=@Branch or Publication=@Publication";

            using (SqlCommand cmd = new SqlCommand(query, con))
            {
                con.Open();

                cmd.Parameters.AddWithValue("@Branch", oDaoBook.Branch);
                cmd.Parameters.AddWithValue("@Publication", oDaoBook.Publication);

                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(dt);
            }
            con.Close();
            return dt;
        }

        internal DataTable SelectedBook(DaoBook oDaoBook)
        {

            SqlConnection con = new SqlConnection(CS);

            string query = "Select *from TblBook where BookName=@BookName and Publication=@Publication";

            using (SqlCommand cmd = new SqlCommand(query, con))
            {
                con.Open();

                cmd.Parameters.AddWithValue("@BookName", oDaoBook.BookName);
                cmd.Parameters.AddWithValue("@Publication", oDaoBook.Publication);

                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(dt);
            }
            con.Close();
            return dt;
        }

        internal DataTable SelectedBook2(DaoBook oDaoBook)
        {

            SqlConnection con = new SqlConnection(CS);

            string query = "Select *from TblBook where BookName=@BookName";

            using (SqlCommand cmd = new SqlCommand(query, con))
            {
                con.Open();

                cmd.Parameters.AddWithValue("@BookName", oDaoBook.BookName);

                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(dt);
            }
            con.Close();
            return dt;
        }

        internal bool UpdateQ(DaoBook oDaoBook)
        {
            SqlConnection con = new SqlConnection(CS);

            string query = "Update TblBook set Quantity=@Quantity where BookName=@BookName";

            using (SqlCommand cmd = new SqlCommand(query, con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@BookName", oDaoBook.BookName);
                cmd.Parameters.AddWithValue("@Quantity", oDaoBook.Quantity);

                int AffectedRow = cmd.ExecuteNonQuery();

                if (AffectedRow >= 1)
                {

                    QuantityUpdate = true;
                }

            }
            con.Close();

            return QuantityUpdate;
        }

        internal DataTable UpdateData(DaoBook oDaoBook)
        {

            SqlConnection con = new SqlConnection(CS);

            string query = "Select *from TblBook where BookName=@BookName";

            using (SqlCommand cmd = new SqlCommand(query, con))
            {
                con.Open();

                cmd.Parameters.AddWithValue("@BookName", oDaoBook.BookName);

                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(dt);
            }
            con.Close();
            return dt;
        }

        internal DataTable ShowBook(DaoBook oDaoBook)
        {
            SqlConnection con = new SqlConnection(CS);

            string query = "Select Id,BookName from TblBook where Publication=@Publication";

            using (SqlCommand cmd = new SqlCommand(query, con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@Publication", oDaoBook.Publication);

                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(dt);

            }
            con.Close();

            return dt;
        }

        internal DataTable BookDetails(DaoBook oDaoBook)
        {

            SqlConnection con = new SqlConnection(CS);

            string query = "Select *from TblBook where Id=@Id";

            using (SqlCommand cmd = new SqlCommand(query, con))
            {
                con.Open();

                cmd.Parameters.AddWithValue("@Id", oDaoBook.Id);
              

                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(dt);
            }
            con.Close();
            return dt;
        }

        internal bool DeleteBook (DaoBook oDaoBook)
        {
            SqlConnection con = new SqlConnection(CS);

            string query = "delete from TblBook where Id=@Id";

            using (SqlCommand cmd = new SqlCommand(query, con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@Id", oDaoBook.Id);

                int AffectedRow = cmd.ExecuteNonQuery();

                if (AffectedRow >= 1)
                {
                    status = true;
                }
            }
            con.Close();

           return status;
        }


    }
}