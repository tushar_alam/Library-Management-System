﻿using LibraryManagementSystem.Dao;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace LibraryManagementSystem.Dal
{

    public class DalIssueBook
    {
        bool status = false;
        
        DataTable dt = new DataTable();

        private static readonly string CS = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;


        internal bool IssueBook(DaoIssueBook oDaoIssueBook)
        {
            SqlConnection con = new SqlConnection(CS);

            string query = "insert into tblIssueBook (BookName,Publication,Branch,StudentName,Date,Days) values (@BookName,@Publication,@Branch,@StudentName,@Date,@Days)";

            using (SqlCommand cmd = new SqlCommand(query, con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@BookName",oDaoIssueBook.BookName);
                cmd.Parameters.AddWithValue("@Publication", oDaoIssueBook.Publication);
                cmd.Parameters.AddWithValue("@Branch", oDaoIssueBook.Branch);
                cmd.Parameters.AddWithValue("@StudentName", oDaoIssueBook.StudentName);
                cmd.Parameters.AddWithValue("@Date", oDaoIssueBook.Date);
                cmd.Parameters.AddWithValue("@Days", oDaoIssueBook.Days);

                int AffectedRow = cmd.ExecuteNonQuery();

                if(AffectedRow>=1)
                {
                    status = true;

                }

            }
            con.Close();
         return status;
        }

        internal DataTable GetBookName(DaoIssueBook oDaoIssueBook)
        {
            SqlConnection con = new SqlConnection(CS);
            string query = "Select * from tblIssueBook where StudentName=@StudentName";

            using (SqlCommand cmd = new SqlCommand(query, con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@StudentName", oDaoIssueBook.StudentName);
             
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(dt);
            }
            con.Close();
            return dt;
        }

        internal DataTable GetStudentName()
        {
            SqlConnection con = new SqlConnection(CS);
            string query = "Select * from tblIssueBook";

            using (SqlCommand cmd = new SqlCommand(query, con))
            {
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(dt);

            }
            con.Close();
            return dt;
        }

        internal bool ReturnBook(DaoIssueBook oDaoIssueBook)
        {
            SqlConnection con = new SqlConnection(CS);

            string query = "delete from tblIssueBook where BookName=@BookName";

            using (SqlCommand cmd = new SqlCommand(query, con))
            {
                con.Open();
                //cmd.Parameters.AddWithValue("@StudentName", oDaoIssueBook.StudentName);
                cmd.Parameters.AddWithValue("@BookName", oDaoIssueBook.BookName);

                int AffectedRow = cmd.ExecuteNonQuery();
                if(AffectedRow>=1)
                {
                    status = true;
                }

            }
            con.Close();
                return status;
        }

        internal DataTable SelectBook(DaoIssueBook oDaoIssueBook)
        {
            SqlConnection con = new SqlConnection(CS);
            string query = "Select * from tblIssueBook where StudentName=@StudentName and BookName=@BookName";

            using (SqlCommand cmd = new SqlCommand(query, con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@StudentName", oDaoIssueBook.StudentName);
                cmd.Parameters.AddWithValue("@BookName", oDaoIssueBook.BookName);

                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(dt);
            }
            con.Close();
            return dt;
        }

    }
}