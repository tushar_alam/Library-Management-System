﻿using LibraryManagementSystem.Dao;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace LibraryManagementSystem.Dal
{
    public class DalStudent
    {
        private static readonly string CS=ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
        bool status = false;
        DataTable dt = new DataTable();


        internal bool AddStudent(DaoStudent oDaoStudent)
        {
            SqlConnection con = new SqlConnection(CS);
            string query = "insert into tblStudent (StudentName,Branch,Gender,DateOfBirth,Mobile,Address,City,Pincode,Email,Password,Photo) values(@StudentName,@Branch,@Gender,@DateOfBirth,@Mobile,@Address,@City,@Pincode,@Email,@Password,@Photo)";

            using (SqlCommand cmd = new SqlCommand(query, con))
            {
                con.Open();

                cmd.Parameters.AddWithValue("@StudentName", oDaoStudent.StudentName);
                cmd.Parameters.AddWithValue("@Branch", oDaoStudent.Branch);
                cmd.Parameters.AddWithValue("@Gender", oDaoStudent.Gender);
                cmd.Parameters.AddWithValue("@DateOfBirth", oDaoStudent.DateOfBirth);
                cmd.Parameters.AddWithValue("@Mobile", oDaoStudent.Mobile);
                cmd.Parameters.AddWithValue("@Address", oDaoStudent.Address);
                cmd.Parameters.AddWithValue("@City", oDaoStudent.City);
                cmd.Parameters.AddWithValue("@Pincode", oDaoStudent.Pincode);

                cmd.Parameters.AddWithValue("@Email", oDaoStudent.Email);
                cmd.Parameters.AddWithValue("@Password", oDaoStudent.Password);
                cmd.Parameters.AddWithValue("@Photo", oDaoStudent.Photo);

                int AffectedRow = cmd.ExecuteNonQuery();

                if (AffectedRow >= 1)
                {
                    status = true;
                }
            }
            con.Close();

            return status;
        }

        internal DataTable ViewStudent(DaoStudent oDaoStudent)
        {
            SqlConnection con = new SqlConnection(CS);

            string query = "Select Id,StudentName,Branch,Gender,DateOfBirth,Mobile,Email from tblStudent where Branch=@Branch or StudentName=@StudentName";

            using (SqlCommand cmd = new SqlCommand(query, con))
            {
                con.Open();

                cmd.Parameters.AddWithValue("@Branch", oDaoStudent.Branch);
                cmd.Parameters.AddWithValue("@StudentName", oDaoStudent.StudentName);

                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(dt);
            }
            con.Close();


            return dt;
        }

        internal DataTable ShowStudent(DaoStudent oDaoStudent)
        {
            SqlConnection con = new SqlConnection(CS);

            string query = "Select Id,StudentName from tblStudent where Branch=@Branch";

            using (SqlCommand cmd = new SqlCommand(query, con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@Branch", oDaoStudent.Branch);

                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(dt);

            }
            con.Close();
            return dt;
        }

        internal DataTable StudentDetails(DaoStudent oDaoStudent)
        {

            SqlConnection con = new SqlConnection(CS);

            string query = "Select * from tblStudent where Id=@Id";

            using (SqlCommand cmd = new SqlCommand(query, con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@Id", oDaoStudent.Id);

                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(dt);

            }
            con.Close();

             return dt;
        }
    }
}