﻿using LibraryManagementSystem.Dao;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace LibraryManagementSystem.Dal
{
    public class DalBranch
    {
        bool status;
        DataTable dt = new DataTable();
        private static readonly string CS = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;


        internal bool AddBranch(DaoBranch oDaoBranch)
        {
            SqlConnection con = new SqlConnection(CS);

            string query = "insert into TblBranch (BranchName) values (@BranchName)";
            using (SqlCommand cmd = new SqlCommand(query, con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@BranchName", oDaoBranch.BranchName);


                int AffectedRow = cmd.ExecuteNonQuery();

                if (AffectedRow >= 1)
                {
                    status = true;
                }

            }
            con.Close();
            return status;
        }

        internal DataTable ShowBranch()
        {

            SqlConnection con = new SqlConnection(CS);

            string query = "Select * from TblBranch ";

            using (SqlCommand cmd = new SqlCommand(query, con))
            {
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(dt);
            }
            con.Close();
            return dt;
        }

    }
}