﻿using LibraryManagementSystem.Dal;
using LibraryManagementSystem.Dao;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LibraryManagementSystem
{
    public partial class BookReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                lblMessage.Visible = false;

              
            }
        }

        protected void btnViewBook_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            DalBook oDalBook = new DalBook();

            if (ddlBranchName.Text != "" || ddlPublicationName.Text != "")
            {
                DaoBook oDaoBook = new DaoBook();

                oDaoBook.Branch = ddlBranchName.Text;
                oDaoBook.Publication = ddlPublicationName.Text;


                dt = oDalBook.ViewBook(oDaoBook);
                if (dt.Rows.Count >= 1)
                {
                    lblMessage.Visible = false;
                    grdViewBook.Visible = true;
                    grdViewBook.DataSource = dt;
                    grdViewBook.DataBind();
                }
                else
                {
                    grdViewBook.Visible = false;
                    lblMessage.Visible = true;
                    lblMessage.Text = "Sorry No Book Is Found.";
                }
            
            }
            else
            {
                lblMessage.Visible = true;
                lblMessage.Text = "Please Select The Branch Name Or The Publication Name.";
            }
        }
    }
}