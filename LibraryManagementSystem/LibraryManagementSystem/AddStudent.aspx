﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="AddStudent.aspx.cs" Inherits="LibraryManagementSystem.AddStudent" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container form-horizontal well well-lg" style=" width:auto; height:auto" >
        <div class="row ">
                 <div class="panel-heading bg-primary" style="text-align:center; width:auto; color:#fff">Student Add Form</div>
       
            <div class="col-xs-6 ">
                <div class="form-group" style="margin-top:5%">
                <asp:label CssClass="col-xs-4" id="lblStudentName" runat="server"><b>Student Name</b></asp:label>
                <div class="col-xs-8">
                   <asp:TextBox CssClass="form-control" ID="txtStudentName" runat="server"></asp:TextBox>

                </div>
            </div>

                <div class="form-group" style="margin-top:2%">
                <asp:label CssClass="col-xs-4" id="Label1" runat="server"><b>Branch</b></asp:label>
                <div class="col-xs-8">
                    <asp:DropDownList CssClass="form-control" ID="ddlBranch" runat="server" DataSourceID="SqlDataSource1" DataTextField="BranchName" DataValueField="BranchName">
                        <asp:ListItem Text="Select" Value="0" Selected="True"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:LibraryConnectionString2 %>" SelectCommand="SELECT [BranchName] FROM [TblBranch]"></asp:SqlDataSource>
                </div>
            </div>

                <div class="form-group" style="margin-top:2%">
                <asp:label CssClass="col-xs-4" id="Label2" runat="server"><b>Gender</b></asp:label>
                <div class="col-xs-8">
                    <asp:DropDownList CssClass="form-control" ID="ddlGender" runat="server">
                     <asp:ListItem Text="Select" Value="0" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Male" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Femail" Value="2"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>

                <div class="form-group" style="margin-top:2%">
                <asp:label CssClass=" col-xs-4" id="Label3" runat="server"><b>Date Of Birth</b></asp:label>
                <div class="col-xs-2">
                    <asp:DropDownList CssClass="form-control" ID="ddlDay" runat="server">
                        <asp:ListItem Text="DD" Value="0" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="1" Value="1"></asp:ListItem>
                        <asp:ListItem Text="2" Value="2"></asp:ListItem>
                        <asp:ListItem Text="3" Value="3"></asp:ListItem>
                        <asp:ListItem Text="4" Value="4"></asp:ListItem>
                        <asp:ListItem Text="5" Value="5"></asp:ListItem>
                        <asp:ListItem Text="6" Value="6"></asp:ListItem>
                        <asp:ListItem Text="7" Value="7"></asp:ListItem>
                        <asp:ListItem Text="8" Value="8"></asp:ListItem>
                        <asp:ListItem Text="9" Value="9"></asp:ListItem>
                        <asp:ListItem Text="10" Value="10"></asp:ListItem>
                        <asp:ListItem Text="11" Value="11"></asp:ListItem>
                        <asp:ListItem Text="12" Value="12"></asp:ListItem>
                        <asp:ListItem Text="13" Value="13"></asp:ListItem>
                        <asp:ListItem Text="14" Value="14"></asp:ListItem>
                        <asp:ListItem Text="15" Value="15"></asp:ListItem>
                        <asp:ListItem Text="16" Value="16"></asp:ListItem>
                        <asp:ListItem Text="17" Value="17"></asp:ListItem>
                        <asp:ListItem Text="18" Value="18"></asp:ListItem>
                        <asp:ListItem Text="19" Value="19"></asp:ListItem>
                        <asp:ListItem Text="20" Value="20"></asp:ListItem>
                        <asp:ListItem Text="21" Value="21"></asp:ListItem>
                        <asp:ListItem Text="22" Value="22"></asp:ListItem>
                        <asp:ListItem Text="23" Value="23"></asp:ListItem>
                        <asp:ListItem Text="24" Value="24"></asp:ListItem>
                        <asp:ListItem Text="25" Value="25"></asp:ListItem>
                        <asp:ListItem Text="26" Value="26"></asp:ListItem>
                        <asp:ListItem Text="27" Value="27"></asp:ListItem>
                        <asp:ListItem Text="28" Value="28"></asp:ListItem>

                        <asp:ListItem Text="29" Value="29"></asp:ListItem>
                        <asp:ListItem Text="30" Value="30"></asp:ListItem>
                        <asp:ListItem Text="31" Value="31"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                     <div class="col-xs-3">
                    <asp:DropDownList CssClass="form-control" ID="ddlMonth" runat="server">
                         <asp:ListItem Text="MM" Value="0" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="January" Value="1"></asp:ListItem>
                        <asp:ListItem Text="February" Value="2"></asp:ListItem>
                        <asp:ListItem Text="March" Value="3"></asp:ListItem>
                        <asp:ListItem Text="April" Value="4"></asp:ListItem>
                        <asp:ListItem Text="May" Value="5"></asp:ListItem>
                        <asp:ListItem Text="June" Value="6"></asp:ListItem>
                        <asp:ListItem Text="July" Value="7"></asp:ListItem>
                        <asp:ListItem Text="August" Value="8"></asp:ListItem>
                        <asp:ListItem Text="September" Value="9"></asp:ListItem>
                        <asp:ListItem Text="October" Value="10"></asp:ListItem>
                        <asp:ListItem Text="November" Value="11"></asp:ListItem>
                        <asp:ListItem Text="December" Value="12"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                     <div class="col-xs-3">
                     <asp:TextBox placeholder="YYYY" CssClass="form-control" ID="txtYear" runat="server"></asp:TextBox>
                    </div>
            </div>
                
                <div class="form-group" style="margin-top:2%">
                <asp:label CssClass="col-xs-4" id="Label4" runat="server"><b>Mobile</b></asp:label>
                <div class="col-xs-8">
                   <asp:TextBox CssClass="form-control" ID="txtMobile" runat="server"></asp:TextBox>

                </div>
            </div>

                <div class="form-group" style="margin-top:2%">
                <asp:label CssClass="col-xs-4" id="Label5" runat="server"><b>Address</b></asp:label>
                <div class="col-xs-8">
                   <asp:TextBox CssClass="form-control" ID="txtAddress" runat="server"></asp:TextBox>

                </div>
            </div>

                <div class="form-group" style="margin-top:2%">
                <asp:label CssClass="col-xs-4" id="Label6" runat="server"><b>City</b></asp:label>
                <div class="col-xs-8">
                   <asp:TextBox CssClass="form-control" ID="txtCity" runat="server"></asp:TextBox>

                </div>
            </div>

                <div class="form-group" style="margin-top:2%">
                <asp:label CssClass="col-xs-4" id="Label7" runat="server"><b>Pincode</b></asp:label>
                <div class="col-xs-8">
                   <asp:TextBox CssClass="form-control" ID="txtPincode" runat="server"></asp:TextBox>
                </div>
            </div>

                <div class="form-group" style="margin-top:2%">
                <asp:label CssClass="col-xs-4" id="Label8" runat="server"><b>Email</b></asp:label>
                <div class="col-xs-8">
                   <asp:TextBox CssClass="form-control" ID="txtEmail" runat="server"></asp:TextBox>

                </div>
            </div>

                <div class="form-group" style="margin-top:2%">
                <asp:label CssClass="col-xs-4" id="Label9" runat="server"><b>Password</b></asp:label>
                <div class="col-xs-8">
                   <asp:TextBox CssClass="form-control" ID="txtPassword" runat="server"></asp:TextBox>

                </div>
            </div>

                <div class="form-group" style="margin-top:2%">
                <asp:label CssClass="col-xs-4" id="Label10" runat="server"><b>Photo</b></asp:label>
                <div class="col-xs-8">
                    <asp:FileUpload CssClass="form-control" ID="filePhoto" runat="server" />

                </div>
            </div>

                <div class="form-group" style="margin-top:2%">
                <div class="col-xs-4"> 
                </div>
                <div class="col-xs-8">
               <asp:Button class="btn btn-success" ID="btnAddStudent" runat="server" Text="Add Student" OnClick="btnAddStudent_Click"  />

                </div>
            </div>
                
                <div class="form-group" style="margin-top:2%">
                <div class="col-xs-4"> 
                </div>
                <div class="col-xs-8">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
                </div>
            </div>

                

            </div>
        </div>
    </div>

</asp:Content>
