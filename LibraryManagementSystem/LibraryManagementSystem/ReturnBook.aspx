﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="ReturnBook.aspx.cs" Inherits="LibraryManagementSystem.ReturnBook" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container well well-lg" style="width:auto">
           <div class="row">
            <div class="col-xs-12" >
                  <div class="panel-heading bg-primary" style="text-align:center; margin-bottom:3%; width:auto; color:#fff">Return Book Form</div>
       

                <div class="form-group" style="margin-top:2%">
                <asp:label CssClass="col-xs-2" id="Label1" runat="server"><b>Select Student</b></asp:label>
                <div class="col-xs-4">
            
                    <asp:DropDownList CssClass="form-control" ID="ddlStudent" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlStudent_SelectedIndexChanged" >
                  
                    <asp:ListItem text="Select One" Value="-1" runat="server">
                      
                   </asp:ListItem>
                    </asp:DropDownList>
              
               </div>
            </div>

                <div class="form-group" style="margin-top:2%">
                <asp:label CssClass="col-xs-2" id="Label2" runat="server"><b>Select Book</b></asp:label>
                <div class="col-xs-4">
                    <asp:dropdownlist class="form-control" id="ddlBook" runat="server" ></asp:dropdownlist>
 
               </div>
            </div>

                <div class="form-group" style="margin-top:5%">
                <div class="col-xs-2"><asp:Button CssClass="btn btn-success" ID="btnSelectBook" Text="Select" runat="server" OnClick="btnSelectBook_Click"/></div>
                <div class="col-xs-4">
                 <asp:label id="lblMessage" runat="server"></asp:label>
               </div>
            </div>

            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <asp:Repeater ID="rptrBookData" runat="server">
                   <ItemTemplate>
                        <asp:Panel style=" border:2px outset cadetblue; margin:5px; float:left; padding:5px; height:250px; width:350px; color:#fff; background-color:darkcyan" ID="pnlBookData" runat="server">              

                                        <table style="margin-left:5px">
                                             <tr>
                                                 <td><b>Book Name:</b></td>
                                                 <td>
                                                     <asp:Label ID="lblBookName" Text='<%#Eval("BookName") %>' runat="server"></asp:Label>
                                                 </td>
                                          </tr>
                                            <%--<tr>
                                                <td><b>Publication</b></td>
                                                <td>
                                                    <asp:Label ID="Label1" runat="server" Text='<%#Eval("Author") %>'></asp:Label>
                                                </td>
                                            </tr>--%>
                                            <tr>
                                                <td><b>Publication: </b></td>
                                                <td>
                                                    <asp:Label ID="Label2" runat="server" Text='<%#Eval("Publication") %>'></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>Branch: </b>

                                                </td>
                                                <td>
                                                    <asp:Label ID="Label3" runat="server" Text='<%#Eval("Branch") %>'></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>Student Name: </b>

                                                </td>
                                                <td>
                                                    <asp:Label ID="Label4" runat="server" Text='<%#Eval("StudentName") %>'></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>Date: </b>
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label5" runat="server" Text='<%#Eval("Date") %>'></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>Days: </b>

                                                </td>
                                                <td>
                                                    <asp:Label ID="Label6" runat="server" Text='<%#Eval("Days") %>'></asp:Label>
                                                </td>
                                            </tr>
                                          <%--  <tr>
                                                <td>
                                                    <b>Battery: </b>

                                                </td>
                                                <td>
                                                    <asp:Label ID="Label7" runat="server" Text='<%#Eval("Battery") %>'></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>Price: </b>

                                                </td>
                                                <td>
                                                    <asp:Label ID="Label8" runat="server" Text='<%#Eval("price") %>'>Tk</asp:Label>
                                                </td>
                                            </tr>--%>
                                        </table>


                        </asp:Panel>

                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>


        <div class="row">
            <div class="col-xs-12">
                <div class="form-group" style="margin-top:2%">
                <div class=" col-xs-offset-2 col-xs-2"><asp:Button ID="btnReturnBook" CssClass="btn btn-success" Text="Return Book" runat="server" OnClick="btnReturnBook_Click" /></div>
               <div class="col-xs-4">
                 <asp:label id="lblMessage2" runat="server"></asp:label>
                     </div>
            </div>
            </div>
        </div>

    </div>


</asp:Content>
