﻿using LibraryManagementSystem.Dal;
using LibraryManagementSystem.Dao;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LibraryManagementSystem
{
    public partial class ReturnBook : System.Web.UI.Page
    {
        bool status = false;
        bool QuantityUpdate = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                lblMessage.Visible = false;
                lblMessage2.Visible = false;
                DataTable dt = new DataTable();
                DalIssueBook oDalIssueBook = new DalIssueBook();
                DaoIssueBook oDaoIssueBook = new DaoIssueBook();


                dt = oDalIssueBook.GetStudentName();

                ddlStudent.DataSource = dt;
                ddlStudent.DataTextField = "StudentName";
                ddlStudent.DataValueField = "Id";
                ddlStudent.DataBind();

            }

        }

        protected void btnSelectBook_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            DalIssueBook oDalIssueBook = new DalIssueBook();
            DaoIssueBook oDaoIssueBook = new DaoIssueBook();

            oDaoIssueBook.StudentName = ddlStudent.SelectedItem.Text;
            oDaoIssueBook.BookName = ddlBook.SelectedItem.Text;

            dt = oDalIssueBook.SelectBook(oDaoIssueBook);

            if (dt.Rows.Count >= 1)
            {
                rptrBookData.DataSource = dt;
                rptrBookData.DataBind();

            }

        }

        protected void btnReturnBook_Click(object sender, EventArgs e)
        {
            if(ddlStudent.SelectedItem.Text!=""&&ddlBook.SelectedItem.Text!="")
            {
                DaoIssueBook oDaoIssueBook = new DaoIssueBook();
                DalIssueBook oDalIssueBook = new DalIssueBook();
                DaoBook oDaoBook = new DaoBook();
                DalBook oDalBook = new DalBook();
                DataTable dt = new DataTable();

                oDaoBook.BookName = ddlBook.SelectedItem.Text;


                dt = oDalBook.SelectedBook2(oDaoBook);

                int Quentity = Convert.ToInt32(dt.Rows[0]["Quantity"].ToString());
                Quentity = Quentity + 1;

                oDaoBook.Quantity = Quentity;
                oDaoBook.BookName = ddlBook.SelectedItem.Text;




                QuantityUpdate = oDalBook.UpdateQ(oDaoBook);
            
                dt = oDalBook.UpdateData(oDaoBook);

                if (QuantityUpdate == true && dt.Rows.Count >= 1)
                {

                    //rptrBookData2.DataSource = dt;
                    //rptrBookData2.DataBind();
                }
                else
                {
                    lblMessage2.Visible = true;
                    lblMessage2.Text = "Ops Something is wrong...";
                }


                oDaoIssueBook.StudentName = ddlStudent.SelectedItem.Text;
                oDaoIssueBook.BookName = ddlBook.SelectedItem.Text;

                status = oDalIssueBook.ReturnBook(oDaoIssueBook);

                if (status == true)
                {
                    lblMessage2.Visible = true;
                    lblMessage2.Text = "This Book is successfully returned.";
                }
                else
                {
                    lblMessage2.Visible = true;
                    lblMessage2.Text = "Ops Somthing is wrong..";
                }
            }
            else
            {
                lblMessage.Visible = true;
                lblMessage.Text = "Please select the student and book name first.";
            }



        }
        protected void ddlStudent_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            DalIssueBook oDalIssueBook = new DalIssueBook();
            DaoIssueBook oDaoIssueBook = new DaoIssueBook();

            oDaoIssueBook.StudentName = ddlStudent.SelectedItem.Text;

            dt = oDalIssueBook.GetBookName(oDaoIssueBook);

            if (dt.Rows.Count >= 1)
            {
                ddlBook.DataSource = dt;
                ddlBook.DataTextField = "BookName";
                ddlBook.DataValueField = "Id";
                ddlBook.DataBind();


            }
        }
    }
}