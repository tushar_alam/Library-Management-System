﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="AddBook.aspx.cs" Inherits="LibraryManagementSystem.AddBook" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div class="container well well-lg" style=" width:auto; height:auto">
    <div class="row" >
      
         <div class="col-xs-12">
              <div class="panel-heading bg-primary" style="text-align:center; width:auto; color:#fff"> Book Add Form</div>
       

            <div class="form-group" style="margin-top:5%">
                <asp:label CssClass="col-xs-2" id="lblBookName" runat="server"><b>Book Name</b></asp:label>
                <div class="col-xs-4">
                   <asp:TextBox CssClass="form-control" ID="txtBookName" runat="server"></asp:TextBox>

                </div>
            </div><br />

            <div class="form-group" style="margin-top:5%">
                <asp:label CssClass="col-xs-2" id="Label1" runat="server"><b>Details</b></asp:label>
                <div class="col-xs-4">
                  <asp:TextBox placeholder="Please write book details here" ToolTip="Please write book details here" TextMode="MultiLine" MaxLength="500" Style="height: 50px;" ID="txtDetails" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div><br />

            <div class="form-group" style="margin-top:5%">
                <asp:label CssClass="col-xs-2" id="Label2" runat="server"><b>Author</b></asp:label>
                <div class="col-xs-4">
                   <asp:TextBox CssClass="form-control" ID="txtAuthor" runat="server"></asp:TextBox>

                </div>
            </div><br />
           
            <div class="form-group" style="margin-top:5%">
                <asp:label CssClass="col-xs-2" id="Label3" runat="server"><b>Publication</b></asp:label>
                <div class="col-xs-4">
                    <asp:DropDownList class="form-control" ID="ddlPublication" runat="server" DataSourceID="SqlDataSource1" DataTextField="PublicationName" DataValueField="PublicationName">
                 
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:LibraryConnectionString2 %>" SelectCommand="SELECT [PublicationName] FROM [PublicationTable]"></asp:SqlDataSource>
                </div>
            </div><br />
            
            <div class="form-group" style="margin-top:5%">
                <asp:label CssClass="col-xs-2" id="Label4" runat="server"><b>Branch</b></asp:label>
                <div class="col-xs-4">
               <asp:DropDownList class="form-control" ID="ddlBranch" runat="server" DataSourceID="SqlDataSource2" DataTextField="BranchName" DataValueField="BranchName">
                 
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:LibraryConnectionString2 %>" SelectCommand="SELECT [BranchName] FROM [TblBranch]"></asp:SqlDataSource>
                </div>
            </div><br />
            
            <div class="form-group" style="margin-top:5%">
                <asp:label CssClass="col-xs-2" id="Label5" runat="server"><b>Price</b></asp:label>
                <div class="col-xs-4">
                   <asp:TextBox CssClass="form-control" ID="txtPrice" runat="server"></asp:TextBox>

                </div>
            </div><br />
            
            <div class="form-group" style="margin-top:5%">
                <asp:label CssClass="col-xs-2" id="Label6" runat="server"><b>Quantity</b></asp:label>
                <div class="col-xs-4">
                   <asp:TextBox CssClass="form-control" ID="txtQuantity" runat="server"></asp:TextBox>

                </div>
            </div><br />
            
            <div class="form-group" style="margin-top:5%">
                <asp:label CssClass="col-xs-2" id="Label7" runat="server"><b>BooK Photo</b></asp:label>
                <div class="col-xs-4">
                    <asp:FileUpload ID="FileUpload1" runat="server" />
                </div>
            </div><br />

            
            </div>

        <div class="row">
            <div class="col-xs-2"></div>
            <div class="col-xs-4">
             <asp:label id="lblMessage" runat="server"></asp:label><br />
            <asp:Button CssClass="btn btn-success" ID="btnAddBook" Text="Add Book" runat="server" onclick="btnAddBook_Click" />
           
            </div>
        </div>
    </div>
</div>

</asp:Content>
