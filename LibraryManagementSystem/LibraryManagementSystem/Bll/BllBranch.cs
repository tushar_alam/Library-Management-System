﻿using LibraryManagementSystem.Dal;
using LibraryManagementSystem.Dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryManagementSystem.Bll
{
    public class BllBranch
    {
        bool status = false;

        internal bool AddBranch(DaoBranch oDaoBranch)
        {
            DalBranch oDalBranch = new DalBranch();

            status = oDalBranch.AddBranch(oDaoBranch);

            return status;

        }
      
    }
}