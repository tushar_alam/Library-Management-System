﻿using LibraryManagementSystem.Dal;
using LibraryManagementSystem.Dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryManagementSystem.Bll
{
    public class BllLogin
    {

        bool status = false;

        internal bool FindMailPass(DaoLogin oDaoLogin)
        {
            DalLogin oDalLogin = new DalLogin();

            status = oDalLogin.FindMailPass(oDaoLogin);

            return status;
        }
    }
}