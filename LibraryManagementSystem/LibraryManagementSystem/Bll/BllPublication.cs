﻿using LibraryManagementSystem.Dal;
using LibraryManagementSystem.Dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryManagementSystem.Bll
{
    public class BllPublication
    {
        bool status;
        DaoPublication oDaoPub = new DaoPublication();
        DalPublication oDalPub = new DalPublication();

        internal bool AddPublicationName(DaoPublication oDaoPub)
        {

            status = oDalPub.AddPublicationName(oDaoPub);

            return status;
        }
    }
}