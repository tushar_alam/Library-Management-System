﻿using LibraryManagementSystem.Dal;
using LibraryManagementSystem.Dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryManagementSystem.Bll
{
    public class BllBook
    {
        bool status = false;

        internal bool AddBook(DaoBook oDaoBook)
        {
            DalBook oDalBook = new DalBook();

            status = oDalBook.AddBook(oDaoBook);

            return status;
        }

    }
}