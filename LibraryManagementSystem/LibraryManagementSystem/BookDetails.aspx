﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="BookDetails.aspx.cs" Inherits="LibraryManagementSystem.BookDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <asp:Repeater ID="rptrBookData" runat="server">
                    <ItemTemplate>
                        <asp:Panel Style="border: 2px outset cadetblue; margin: 5px; float: left; padding: 5px; height: 250px; width: 350px; color: #fff; background-color: cornflowerblue" ID="pnlBookData" runat="server">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblBookName" Text='<%#Eval("BookName") %>' runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Image ID="Image1" ImageUrl='<%#Eval("Image")%>' Style="width: 175px; height: 210px" runat="server" />
                                    </td>
                                    <td>
                                        <table style="margin-left: 5px">
                                            <tr>
                                                <td><b>Autor</b></td>
                                                <td>
                                                    <asp:Label ID="Label1" runat="server" Text='<%#Eval("Author") %>'></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><b>Publication: </b></td>
                                                <td>
                                                    <asp:Label ID="Label2" runat="server" Text='<%#Eval("Publication") %>'></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>Branch: </b>

                                                </td>
                                                <td>
                                                    <asp:Label ID="Label3" runat="server" Text='<%#Eval("Branch") %>'></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>Price: </b>

                                                </td>
                                                <td>
                                                    <asp:Label ID="Label4" runat="server" Text='<%#Eval("Price") %>'></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>Quantity: </b>
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label5" runat="server" Text='<%#Eval("Quantity") %>'></asp:Label>
                                                </td>
                                            </tr>
                                            <%-- <tr>
                                                <td>
                                                    <b>Ram: </b>

                                                </td>
                                                <td>
                                                    <asp:Label ID="Label6" runat="server" Text='<%#Eval("ram") %>'></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>Battery: </b>

                                                </td>
                                                <td>
                                                    <asp:Label ID="Label7" runat="server" Text='<%#Eval("Battery") %>'></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>Price: </b>

                                                </td>
                                                <td>
                                                    <asp:Label ID="Label8" runat="server" Text='<%#Eval("price") %>'>Tk</asp:Label>
                                                </td>
                                            </tr>--%>
                                        </table>
                                    </td>
                                </tr>
                            </table>

                        </asp:Panel>

                    </ItemTemplate>
                </asp:Repeater>
                <asp:Button CssClass="btn-danger" ID="btnDelete" runat="server" Text="Delete" OnClick="btnDelete_Click" />
                <asp:Label ID="lblMessage2" runat="server"></asp:Label>
            </div>
        </div>
    </div>


</asp:Content>
