﻿using LibraryManagementSystem.Dal;
using LibraryManagementSystem.Dao;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LibraryManagementSystem
{
    public partial class StudentReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                lblMessage.Visible = false;
                
            }

        }

        protected void btnViewStudent_Click(object sender, EventArgs e)
        {
       
            DataTable dt = new DataTable();
            DalStudent oDalStudent = new DalStudent();

            if (ddlBranch.SelectedItem.Text != "" || ddlStudentName.SelectedItem.Text != "")
            {
                DaoStudent oDaoStudent = new DaoStudent();

                oDaoStudent.Branch = ddlBranch.SelectedItem.Text;
                oDaoStudent.StudentName = ddlStudentName.SelectedItem.Text;


                dt = oDalStudent.ViewStudent(oDaoStudent);

                if(dt.Rows.Count>=1)
                {
                 
                    grdViewStudent.DataSource = dt;
                    grdViewStudent.DataBind();

                }
                else
                {
                    lblMessage.Visible = true;
                    lblMessage.Text = "Sorry No Student Found.";
                }
              
            }
            else
            {
                lblMessage.Visible = true;
                lblMessage.Text = "Please Select The Branch Name Or The Student Name.";
            }


        
    }
    }
}