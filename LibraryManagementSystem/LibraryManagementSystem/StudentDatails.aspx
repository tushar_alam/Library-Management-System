﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="StudentDatails.aspx.cs" Inherits="LibraryManagementSystem.StudentDatails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:Label ID="lblMesage" runat="server"></asp:Label>


    <asp:Repeater ID="rptrstudentData" runat="server">
                    <ItemTemplate>
                        <asp:Panel Style="border:2px outset cadetblue; margin: 5px; float: left; padding: 5px; height: 290px; width: 390px; color: #fff; background-color:cornflowerblue" ID="pnlBookData" runat="server">
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblStudentName" Text='<%#Eval("StudentName") %>' runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Image ID="Image1" ImageUrl='<%#Eval("Photo")%>' Style="width: 175px; height: 230px" runat="server" />
                                    </td>
                                    <td>
                                        <table style="margin-left: 5px">
                                            <tr>
                                                <td><b>Branch</b></td>
                                                <td>
                                                    <asp:Label ID="Label1" runat="server" Text='<%#Eval("Branch") %>'></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><b>Gander: </b></td>
                                                <td>
                                                    <asp:Label ID="Label2" runat="server" Text='<%#Eval("Gender") %>'></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>Date Of Birth: </b>

                                                </td>
                                                <td>
                                                    <asp:Label ID="Label3" runat="server" Text='<%#Eval("DateOfBirth") %>'></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>Mobile: </b>

                                                </td>
                                                <td>
                                                    <asp:Label ID="Label4" runat="server" Text='<%#Eval("Mobile") %>'></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>Email: </b>
                                                </td>
                                                <td>
                                                    <asp:Label ID="Label5" runat="server" Text='<%#Eval("Email") %>'></asp:Label>
                                                </td>
                                            </tr>
                                          
                                        </table>
                                    </td>
                                </tr>
                            </table>

                        </asp:Panel>

                    </ItemTemplate>
                </asp:Repeater>


</asp:Content>
