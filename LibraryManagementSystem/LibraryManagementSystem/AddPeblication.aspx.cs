﻿using LibraryManagementSystem.Bll;
using LibraryManagementSystem.Dal;
using LibraryManagementSystem.Dao;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LibraryManagementSystem
{
    public partial class AddPeblication : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                lblMessage.Visible = false;
            }

        }

        protected void btnAddPublication_Click(object sender, EventArgs e)
        {
            if(txtPublicationName.Text!="")
            {
                bool status = false;
                DataTable dt = new DataTable();

                BllPublication oBllPub = new BllPublication();
                DaoPublication oDaoPub = new DaoPublication();
                DalPublication oDalpub = new DalPublication();


                oDaoPub.PublicationName = txtPublicationName.Text;

                status = oBllPub.AddPublicationName(oDaoPub);
                dt = oDalpub.ShowPubData();

                if (status == true )
                {
                    lblMessage.Visible = true;
                    lblMessage.Text = "Publication Inserted.";

                }
                else
                {
                    lblMessage.Visible = true;
                    lblMessage.Text = "Ops something is wrong...";
                }
                if(dt.Rows.Count >=1)
                {
                    GridView1.DataSourceID ="SqlDataSource1";
                    GridView1.DataBind();
                }

            }
            else
            {
                lblMessage.Visible = true;
                lblMessage.Text = "Please Enter the name of Publication.";
            }
           

        }
    }
}