﻿using LibraryManagementSystem.Bll;
using LibraryManagementSystem.Dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LibraryManagementSystem
{
    public partial class AddBook : System.Web.UI.Page
    {
        string path;
        string name;
        bool status = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                lblMessage.Visible = false;

            }

        }

        protected void btnAddBook_Click(object sender, EventArgs e)
        {
           

            if (txtBookName.Text != "" && txtDetails.Text != "" && txtAuthor.Text != "" && txtPrice.Text != "" && txtQuantity.Text != "" && ddlBranch.Text != "" && ddlPublication.Text != "")
            {
                if (!FileUpload1.HasFile)
                {
                    lblMessage.Visible = true;
                    lblMessage.Text = "Please upload an image file";
                }
                if (FileUpload1.HasFile)
                {
                    
                     name = FileUpload1.PostedFile.FileName;
                    string extension = System.IO.Path.GetExtension(FileUpload1.FileName);
                    if (extension.ToLower() == ".jpg" || extension.ToLower() == ".png")
                    {
                        FileUpload1.PostedFile.SaveAs(Server.MapPath(".") + "/images/" + name);
                         path = "/images/" + name.ToString();
                    }

                    DaoBook oDaoBook = new DaoBook();
                    oDaoBook.BookName = txtBookName.Text;
                    oDaoBook.Details = txtDetails.Text;
                    oDaoBook.Author = txtAuthor.Text;
                    oDaoBook.Publication = ddlPublication.Text;
                    oDaoBook.Branch = ddlBranch.Text;
                    oDaoBook.Price = Convert.ToInt32(txtPrice.Text);
                    oDaoBook.Quantity = Convert.ToInt32(txtQuantity.Text);
                    oDaoBook.Image = path;

                    BllBook oBllBook = new BllBook();

                    status = oBllBook.AddBook(oDaoBook);

                    if(status==true)
                    {
                        lblMessage.Visible = true;
                        lblMessage.Text = "This book is successfully added.";
                    }
                    else
                    {
                        lblMessage.Visible = true;
                        lblMessage.Text = "Somthing is wrong...";
                    }
                }
            }
            else
            {
                lblMessage.Visible = true;
                lblMessage.Text = "Please Fill All Of The Given TextBoxes...";
            }

        }
    }
}