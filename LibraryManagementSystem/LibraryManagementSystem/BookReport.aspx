﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="BookReport.aspx.cs" Inherits="LibraryManagementSystem.BookReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container well well-lg" style=" width:auto; height:auto">
      
         <div class="panel-heading bg-primary" style="text-align:center; width:auto; color:#fff">Book Report Form</div>
       
         <div class="row" style="margin-top:5%">              
            <label class="col-xs-3"><b>Select Branch</b></label>
            <div class="col-xs-4">
                <asp:DropDownList CssClass="form-control" ID="ddlBranchName" runat="server" DataSourceID="SqlDataSource1" DataTextField="BranchName" DataValueField="BranchName">             
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:LibraryConnectionString2 %>" SelectCommand="SELECT [BranchName] FROM [TblBranch]"></asp:SqlDataSource>
            </div>
        </div><br />             
         <div class="row">
             <label  class="col-xs-3"><b>Select Publication</b></label>
            <div class="col-xs-4">
                <asp:DropDownList CssClass="form-control" ID="ddlPublicationName" runat="server" DataSourceID="SqlDataSource2" DataTextField="PublicationName" DataValueField="PublicationName">
                   
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:LibraryConnectionString2 %>" SelectCommand="SELECT [PublicationName] FROM [PublicationTable]"></asp:SqlDataSource>
            </div>
        </div><br />
        <div class="row">
             <div  class="col-xs-3"></div>
            <div class="col-xs-4">
                <asp:button Class="btn btn-success" ID="btnViewBook"  text="View" runat="server" OnClick="btnViewBook_Click"></asp:button>
            </div>
        </div>
        <div class="row" style="margin-top:5px">
            <asp:label ID="lblMessage" runat="server"></asp:label>
                 <asp:GridView ID="grdViewBook" runat="server" BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Vertical">
                     <AlternatingRowStyle BackColor="#DCDCDC" />
                     <Columns>
                     <asp:HyperLinkField DataNavigateUrlFields="Id" DataNavigateUrlFormatString="BookDetails.aspx?Id={0}" HeaderText="Click " Text="View" />
                     </Columns>
                     <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                     <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
                     <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                     <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                     <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                     <SortedAscendingCellStyle BackColor="#F1F1F1" />
                     <SortedAscendingHeaderStyle BackColor="#0000A9" />
                     <SortedDescendingCellStyle BackColor="#CAC9C9" />
                     <SortedDescendingHeaderStyle BackColor="#000065" />           
                 </asp:GridView>
        </div>
        <%--<div class="row">
             <div  class="col-xs-7">     
             </div>
        </div>--%>
    </div>
</asp:Content>
