﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="StudentReport.aspx.cs" Inherits="LibraryManagementSystem.StudentReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container well well-lg" style="width: auto; height: auto">

        <div class="row">


            <div class="panel-heading bg-primary" style="text-align: center; margin-bottom: 3%; width: auto; color: #fff">Student Report Form</div>


            <label class="col-xs-3"><b>Select Branch</b></label>
            <div class="col-xs-4">
                <asp:DropDownList CssClass="form-control" ID="ddlBranch" runat="server" DataSourceID="SqlDataSource1" DataTextField="BranchName" DataValueField="BranchName">
                    <asp:ListItem Text="Select" Value="0" Selected="True"></asp:ListItem>
                </asp:DropDownList>

                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:LibraryConnectionString2 %>" SelectCommand="SELECT [BranchName] FROM [TblBranch]"></asp:SqlDataSource>

            </div>
        </div>
        <br />

        <div class="row">
            <label class="col-xs-3"><b>Student Name</b></label>
            <div class="col-xs-4">
                <asp:DropDownList CssClass="form-control" ID="ddlStudentName" runat="server" DataSourceID="SqlDataSource2" DataTextField="StudentName" DataValueField="StudentName">
                    <asp:ListItem Text="Select" Value="0" Selected="True"></asp:ListItem>
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:LibraryConnectionString2 %>" SelectCommand="SELECT [StudentName] FROM [tblStudent]"></asp:SqlDataSource>
            </div>

        </div>
        <br />

        <div class="row">
            <div class="col-xs-3"></div>
            <div class="col-xs-4">
                <asp:Button Class="btn btn-success" ID="btnViewStudent" Text="View" runat="server" OnClick="btnViewStudent_Click"></asp:Button>
            </div>
        </div>
        <div class="row" style="width: auto">
            <div class="col-xs-6" style="width: 1000px">


                <asp:Label ID="lblMessage" runat="server"></asp:Label><br />

                <asp:GridView ID="grdViewStudent" runat="server" BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Vertical">
                    <AlternatingRowStyle BackColor="#DCDCDC" />
                    <Columns>
                        <asp:HyperLinkField DataNavigateUrlFields="Id" DataNavigateUrlFormatString="StudentDatails.aspx?Id={0}" HeaderText="Click" Text="View" />
                    </Columns>
                    <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                    <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#0000A9" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#000065" />
                </asp:GridView>

            </div>
        </div>

    </div>
</asp:Content>
