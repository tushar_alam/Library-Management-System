﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="AddBranch.aspx.cs" Inherits="LibraryManagementSystem.AddBranch" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container well well-lg" style=" width:auto; height:auto ">
        <div class="row">

        <div class="col-xs-12">
             <div class="panel-heading bg-primary" style="text-align:center; width:auto; color:#fff"> Branch Add Form</div>
       
  
            <div class="form-group" style="margin-top:5%">
                <asp:label CssClass="col-xs-2" id="lblBranchName" runat="server"><b>Branch Name</b></asp:label>
                <div class="col-xs-4">
                   <asp:TextBox CssClass="form-control" ID="txtBranchName" runat="server"></asp:TextBox>

                </div>
                <div class="col-xs-2">

            <asp:Button CssClass="btn btn-success" ID="btnAddBranch" Text="Add Branch" runat="server" OnClick="btnAddBranch_Click" />
           
                </div>

            </div>
        </div>
      
        </div>

        <div class="row">
            <div class="col-xs-2"></div>
            <div class="col-xs-4" style="margin-top:5px; margin-left:5px">
                <asp:label id="lblMessage" runat="server"></asp:label>
                 </div>
        </div>

        <div class="row" style="margin-top:5%">
            <div class="col-xs-2"></div>

            <div class="col-xs-6">
          
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataKeyNames="Id" DataSourceID="SqlDataSource1" GridLines="Vertical">
                    <AlternatingRowStyle BackColor="#DCDCDC" />
                <Columns>
                    <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                    <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" ReadOnly="True" SortExpression="Id" />
                    <asp:BoundField DataField="BranchName" HeaderText="BranchName" SortExpression="BranchName" />
                </Columns>
                <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
                <PagerStyle ForeColor="Black" HorizontalAlign="Center" BackColor="#999999" />
                <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                <SortedAscendingHeaderStyle BackColor="#0000A9" />
                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                <SortedDescendingHeaderStyle BackColor="#000065" />
            </asp:GridView>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:LibraryConnectionString2 %>" DeleteCommand="DELETE FROM [TblBranch] WHERE [Id] = @Id" InsertCommand="INSERT INTO [TblBranch] ([BranchName]) VALUES (@BranchName)" SelectCommand="SELECT * FROM [TblBranch]" UpdateCommand="UPDATE [TblBranch] SET [BranchName] = @BranchName WHERE [Id] = @Id">
                <DeleteParameters>
                    <asp:Parameter Name="Id" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="BranchName" Type="String" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="BranchName" Type="String" />
                    <asp:Parameter Name="Id" Type="Int32" />
                </UpdateParameters>
            </asp:SqlDataSource>

            </div>
        </div>
    </div>

</asp:Content>
