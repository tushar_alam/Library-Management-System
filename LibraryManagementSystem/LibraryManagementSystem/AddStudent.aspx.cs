﻿using LibraryManagementSystem.Dal;
using LibraryManagementSystem.Dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LibraryManagementSystem
{
    public partial class AddStudent : System.Web.UI.Page
    {
        string path;
        string name;
        bool status = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                lblMessage.Visible = false;
            }

        }

        protected void btnAddStudent_Click(object sender, EventArgs e)
        {

            if(txtStudentName .Text!=""&& txtAddress.Text!=""&& txtCity.Text!=""&& txtEmail.Text!=""&&txtMobile.Text!=""&& txtPassword.Text!=""&&txtPincode.Text!=""&&txtYear.Text!=""&& ddlBranch.Text!=""&& ddlDay.Text!=""&& ddlGender.Text!=""&&ddlMonth.Text!="")
            {
                if (!filePhoto.HasFile)
                {
                    lblMessage.Visible = true;
                    lblMessage.Text = "Please upload an image file";
                }
                if (filePhoto.HasFile)
                {

                    name = filePhoto.PostedFile.FileName;
                    string extension = System.IO.Path.GetExtension(filePhoto.FileName);
                    if (extension.ToLower() == ".jpg" || extension.ToLower() == ".png")
                    {
                        filePhoto.PostedFile.SaveAs(Server.MapPath(".") + "/images/" + name);
                        path = "/images/" + name.ToString();
                    }
                    DaoStudent oDaoStudent = new DaoStudent();
                    DalStudent odalStudent = new DalStudent();

                    oDaoStudent.StudentName = txtStudentName.Text;
                    oDaoStudent.Branch = ddlBranch.Text;

                    oDaoStudent.StudentName = txtStudentName.Text;
                    oDaoStudent.Branch = ddlBranch.Text;
                    oDaoStudent.DateOfBirth = ddlDay.SelectedItem.Text+"-"+ddlMonth.SelectedItem.Text+"-"+txtYear.Text;
                    oDaoStudent.Gender = ddlGender.SelectedItem.Text;
                    oDaoStudent.Mobile = txtMobile.Text;
                    oDaoStudent.Address = txtAddress.Text;
                    oDaoStudent.City= txtCity.Text;
                    oDaoStudent.Pincode = txtPincode.Text;
                    oDaoStudent.Email =txtEmail.Text;
                    oDaoStudent.Password =txtPassword.Text;
                    oDaoStudent.Photo =path;

                    status = odalStudent.AddStudent(oDaoStudent);

                    if (status == true)
                    {
                        lblMessage.Visible = true;
                        lblMessage.Text = "This Student is successfully added.";
                    }
                    else
                    {
                        lblMessage.Visible = true;
                        lblMessage.Text = "Somthing is wrong...";
                    }
                }



            }
            else
            {
                lblMessage.Visible = true;
                lblMessage.Text = "Please Fill Up The All Text Boxes Given Above.";
            }


        }
    }
}