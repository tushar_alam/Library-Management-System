﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="AddPeblication.aspx.cs" Inherits="LibraryManagementSystem.AddPeblication" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <div class="container well well-lg" style=" width:auto; height:auto " >
        <div class="row">
            <div class="col-xs-12">
                  <div class="panel-heading bg-primary" style="text-align:center; width:auto; color:#fff">Publication Add Form</div>
       
                         <div class="form-group" style="margin-top:5%">
                      <label class="control-label col-xs-2">Publication Name</label>
                    <div class="col-xs-4">
                       <asp:TextBox CssClass="text-primary form-control" ID="txtPublicationName" runat="server"></asp:TextBox>
               
                    </div>
                    <div class="col-xs-2">
                         <asp:Button CssClass="btn btn-success" ID="btnAddPublication" runat="server" Text="Add Publication" OnClick="btnAddPublication_Click"/>
                               
                    </div>
                     </div>
            </div>                
       </div>
       <div class="row" style="margin-top:5px; margin-left:5px">         
              <asp:Label CssClass="col-xs-offset-2" ID="lblMessage"  runat="server"></asp:Label>      
        </div>
       <div class="row">
            <div class="col-xs-offset-2 col-xs-4">
            
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="3" DataKeyNames="Id" DataSourceID="SqlDataSource1" BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" GridLines="Vertical">
                    <AlternatingRowStyle BackColor="#DCDCDC" />
                    <Columns>
                         <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" HeaderText="Action" />
                         <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" ReadOnly="True" SortExpression="Id" Visible="False" />
                         <asp:BoundField DataField="PublicationName" HeaderText="Publication Name" SortExpression="PublicationName" />
                    </Columns>
                   
                    <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                    <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#0000A9" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#000065" />
                   
                      </asp:GridView>
              
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:LibraryConnectionString2 %>" DeleteCommand="DELETE FROM [PublicationTable] WHERE [Id] = @Id" InsertCommand="INSERT INTO [PublicationTable] ([PublicationName]) VALUES (@PublicationName)" SelectCommand="SELECT * FROM [PublicationTable]" UpdateCommand="UPDATE [PublicationTable] SET [PublicationName] = @PublicationName WHERE [Id] = @Id">
                    <DeleteParameters>
                        <asp:Parameter Name="Id" Type="Int32" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="PublicationName" Type="String" />
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="PublicationName" Type="String" />
                        <asp:Parameter Name="Id" Type="Int32" />
                    </UpdateParameters>
                </asp:SqlDataSource>
              
          

            </div>

        </div>
    </div>

  <%--  <div class="panel">
        <div class="panel-heading bg-primary text-center">
            <h4>Add Publication Form</h4>
        </div>

        <div class="panel-body" style="border: 1px solid cornflowerblue">

          <div class="form-group" style="margin-top:5%">
                      <label class="control-label col-xs-2">Publication Name</label>
                    <div class="col-xs-4">
                       <asp:TextBox CssClass="text-primary form-control" ID="TextBox1" runat="server"></asp:TextBox>
               
                    </div>
                    <div class="col-xs-2">
                         <asp:Button CssClass="btn btn-success" ID="Button1" runat="server" Text="Add Publication" OnClick="btnAddPublication_Click"/>
                               
                    </div>
                     </div>

          <div class="row" style="margin-top:5px; margin-left:5px">         
              <asp:Label CssClass="col-xs-offset-2" ID="Label1"  runat="server"></asp:Label>      
        </div>
          <div class="row">
            <div class="col-xs-offset-2 col-xs-4">


                <asp:GridView ID="GridView2" runat="server" AutoPostBack="true" AutoGenerateColumns="False" BackColor="#DEBA84" BorderColor="#DEBA84" BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2" DataKeyNames="Id" DataSourceID="SqlDataSource1" Height="160px" Width="348px">
                    <Columns>
                        <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                        <asp:BoundField DataField="Id" HeaderText="Id" InsertVisible="False" ReadOnly="True" SortExpression="Id" />
                        <asp:BoundField DataField="PublicationName" HeaderText="PublicationName" SortExpression="PublicationName" />
                    </Columns>
                    <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                    <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                    <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" />
                    <RowStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                    <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#FFF1D4" />
                    <SortedAscendingHeaderStyle BackColor="#B95C30" />
                    <SortedDescendingCellStyle BackColor="#F1E5CE" />
                    <SortedDescendingHeaderStyle BackColor="#93451F" />
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:LibraryConnectionString2 %>" DeleteCommand="DELETE FROM [PublicationTable] WHERE [Id] = @Id" InsertCommand="INSERT INTO [PublicationTable] ([PublicationName]) VALUES (@PublicationName)" SelectCommand="SELECT * FROM [PublicationTable]" UpdateCommand="UPDATE [PublicationTable] SET [PublicationName] = @PublicationName WHERE [Id] = @Id">
                    <DeleteParameters>
                        <asp:Parameter Name="Id" Type="Int32" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="PublicationName" Type="String" />
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="PublicationName" Type="String" />
                        <asp:Parameter Name="Id" Type="Int32" />
                    </UpdateParameters>
                </asp:SqlDataSource>

            </div>

        </div>

        </div>
    </div>--%>

</asp:Content>
