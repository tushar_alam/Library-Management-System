﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="LibraryManagementSystem.Login" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Lybrary Management System</title>

    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <script>
        function Validation()
        {
            var ret = true;
            if (document.getElementById("txtEmail").value == "")
            {
                alert("Please enter your email.");
                ret = false;
            }

            if (document.getElementById("txtPassword").value == "")
            {
                alert("Please enter your password.");
                ret = false;
            }

            return ret;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">

        <div class="container">
            <div class="row">
                <div class="col-xs-12" style="background-image: url(images/12.jpg)">
                    
                    <div class="row" style="width: 350px; border-radius: 5px; background-color: rgba(47, 79, 79,0.7); margin: 150px auto; padding: 15px">
                        <div class="">
                            <img src="images/1.png" style="height: 100px; width: 330px" />
                            <h3 style="color: white">Log in to Library</h3>
                            <br />
                            <div class="form-group">
                                <label style="color: white">Email:</label>
                                <asp:TextBox CssClass="form-control required" type="Email" ID="txtEmail" runat="server" placeHolder="Enter email"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label style="color: white">Password:</label>
                                <asp:TextBox Class="form-control required" ID="txtPassword" runat="server" type="password" placeHolder="Enter password"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <asp:CheckBox Style="color: white" ID="cbxRememberMe" runat="server" Text="Remember Me " />
                            </div>
                            <asp:Button CssClass="btn btn-success form-control" ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" OnClientClick="return Validation()"/>
                        </div>
                        <br />
                        <asp:Label Style="color: red" ID="lblmessage" runat="server"></asp:Label>
                        <asp:Button CssClass="btn btn-warning form-control" ID="btnForgotPassword" runat="server" Text="Forgot your password?" />
                    </div>
                </div>
            </div>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>

    </form>
</body>
</html>
