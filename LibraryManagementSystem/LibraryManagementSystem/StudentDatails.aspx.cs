﻿using LibraryManagementSystem.Dal;
using LibraryManagementSystem.Dao;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LibraryManagementSystem
{
    public partial class StudentDatails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                DaoStudent oDaoStudent = new DaoStudent();
                DalStudent oDalStudent = new DalStudent();

                DataTable dt = new DataTable();

               oDaoStudent.Id=Convert.ToInt32(Request.QueryString["Id"]);


               dt= oDalStudent.StudentDetails(oDaoStudent);

               if(dt.Rows.Count>=1)
                {
                    rptrstudentData.DataSource = dt;
                    rptrstudentData.DataBind();
                }
                else
                {
                    lblMesage.Visible = true;
                    lblMesage.Text = "Sorry something is wrong";
                }

            }

        }
    }
}