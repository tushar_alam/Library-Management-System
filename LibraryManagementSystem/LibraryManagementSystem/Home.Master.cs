﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LibraryManagementSystem
{
    public partial class Home : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnWelcome_Click(object sender, EventArgs e)
        {
            Response.Redirect("Welcome.aspx");
        }

        protected void btnAddPublication_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddPeblication.aspx");
        }

        protected void btnAddBook_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddBook.aspx");
        }

        protected void btnBookReport_Click(object sender, EventArgs e)
        {
            Response.Redirect("BookReport.aspx");
        }

        protected void btnAddBranch_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddBranch.aspx");
        }

        protected void btnAddStudent_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddStudent.aspx");
        }

        protected void btnStudentReport_Click(object sender, EventArgs e)
        {
            Response.Redirect("StudentReport.aspx");
        }

        protected void btnIssueBook_Click(object sender, EventArgs e)
        {
            Response.Redirect("IssueBook.aspx");
        }

        protected void btnIssueReport_Click(object sender, EventArgs e)
        {
            Response.Redirect("IssueReport.aspx");
        }

        protected void btnReturnBook_Click(object sender, EventArgs e)
        {
            Response.Redirect("ReturnBook.aspx");
        }

        protected void btnLogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("Login.aspx");
        }
    }
}