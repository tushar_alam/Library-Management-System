﻿using LibraryManagementSystem.Bll;
using LibraryManagementSystem.Dal;
using LibraryManagementSystem.Dao;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LibraryManagementSystem
{
    public partial class AddBranch : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lblMessage.Visible = false;
            }
        }

        protected void btnAddBranch_Click(object sender, EventArgs e)
        {
            if(txtBranchName.Text!="")
            {
                bool status = false;
                DataTable dt = new DataTable();
                DaoBranch oDaoBranch = new DaoBranch();
                BllBranch oBllBranch = new BllBranch();
                DalBranch oDalBranch = new DalBranch();


                oDaoBranch.BranchName = txtBranchName.Text;
                

                status = oBllBranch.AddBranch(oDaoBranch);

                if(status==true)
                {
                    lblMessage.Visible = true;
                    lblMessage.Text = "Branch is successfully Added.";

                }
                else
                {
                    lblMessage.Visible = true;
                    lblMessage.Text = "Branch is not added.";
                }

                dt = oDalBranch.ShowBranch();

                if (dt.Rows.Count>=1)
                {
                    GridView1.DataSourceID= "SqlDataSource1";
                    GridView1.DataBind();
                }
                else
                {
                    lblMessage.Visible = true;
                    lblMessage.Text = "Sorry No Branch is Found.";
                }
            }
            else
            {
                lblMessage.Visible = true;
                lblMessage.Text = "Please Enter the name of Branch.";
            }

        }
    }
}