﻿using LibraryManagementSystem.Dal;
using LibraryManagementSystem.Dao;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LibraryManagementSystem
{
    public partial class BookDetails : System.Web.UI.Page
    {
        bool status = false;
      

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                lblMessage2.Visible = false;

                DataTable dt = new DataTable();
                DalBook oDalBook = new DalBook();
                DaoBook oDaoBook = new DaoBook();

                oDaoBook.Id = Convert.ToInt32(Request.QueryString["Id"]);
             
                dt = oDalBook.BookDetails(oDaoBook);

                if (dt.Rows.Count >= 1)
                {
                    rptrBookData.DataSource = dt;
                    rptrBookData.DataBind();

                }
                else
                {
                    lblMessage.Text = "Sorry something is wrong..";
                }
            }          
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
           
            DalBook oDalBook = new DalBook();
            DaoBook oDaoBook = new DaoBook();

            oDaoBook.Id = Convert.ToInt32(Request.QueryString["Id"]);

            status = oDalBook.DeleteBook(oDaoBook);

            if(status==true)
            {
                rptrBookData.Visible = false;
                lblMessage2.Visible = true;
                lblMessage2.Text = "This book is successfully deleted.";
            }
            else
            {
                lblMessage2.Visible = true;
                lblMessage2.Text = "Ops somethings is wrong...";
            }
           
        }
    }
}