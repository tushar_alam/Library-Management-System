﻿using LibraryManagementSystem.Dal;
using LibraryManagementSystem.Dao;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LibraryManagementSystem
{
    public partial class IssueBook : System.Web.UI.Page
    {
        bool status = false;
        bool QuantityUpdate = false;
       

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
           
                Calendar1.Visible = false;
                lblMessage.Visible = false;
                lblMessage2.Visible = false;
            }
        }

        protected void btnSelectBook_Click(object sender, EventArgs e)
        {
            if (ddlPublication.Text == "" || ddlBook.Text == "")
            {
                lblMessage.Visible = true;
                lblMessage.Text = "Please Select Publication and Book Name.";
                return;
            }

            else if (ddlPublication.Text != "" && ddlBook.Text != "")
            {
                DataTable dt = new DataTable();

                DalBook oDalBook = new DalBook();
                DaoBook oDaoBook = new DaoBook();

                oDaoBook.BookName = ddlBook.SelectedItem.Text;
                oDaoBook.Publication = ddlPublication.SelectedItem.Text;


                dt = oDalBook.SelectedBook(oDaoBook);


                if (dt.Rows.Count >= 1)
                {

                    rptrBookData.DataSource = dt;
                    rptrBookData.DataBind();
                    Session["Quantity"] = dt.Rows[0]["Quantity"].ToString();

                }
                else
                {
                    lblMessage.Visible = true;
                    lblMessage.Text = "Sorry No Book Found";

                }

            }
         

            else
            {
                
                lblMessage.Visible = true;
                lblMessage.Text = "Please Select Publication and Book Name.";
            }

        }

        protected void btnIssueBook_Click(object sender, EventArgs e)
        {
            if(ddlBranch.Text=="" || ddlStudent.Text=="")
            {
                lblMessage2.Visible = true;
                lblMessage2.Text = "Please Select the Branch And Student Name First..";
                return;
            }

            if(txtDays.Text!="")
            {
                int days = Convert.ToInt32(txtDays.Text);
                if (days > 10)
                {
                    lblMessage2.Visible = true;
                    lblMessage2.Text = "More then 10 days is not allowed.";
                    return;
                }
            }
            else
            {
                lblMessage2.Visible = true;
                lblMessage2.Text = "Please enter the day number..";
                return;
            }

           
            if (ddlBranch.SelectedItem.Text != "" && ddlStudent.SelectedItem.Text != "")
            {
                DaoIssueBook oDaoIssueBook = new DaoIssueBook();
                DalIssueBook oDalIssueBook = new DalIssueBook();
                DaoBook oDaoBook = new DaoBook();
                DalBook odalBook = new DalBook();
                DataTable dt = new DataTable();

                int quantity =Convert.ToInt32(Session["Quantity"].ToString());

                quantity = quantity - 1;

                oDaoIssueBook.BookName = ddlBook.SelectedItem.Text;
                oDaoIssueBook.Publication = ddlPublication.SelectedItem.Text;
                oDaoIssueBook.Branch = ddlBranch.SelectedItem.Text;
                oDaoIssueBook.StudentName = ddlStudent.SelectedItem.Text;
                oDaoIssueBook.Date = Calendar1.TodaysDate.ToString("D");
                oDaoIssueBook.Days = txtDays.Text;

                oDaoBook.Quantity = quantity;
                oDaoBook.BookName = ddlBook.SelectedItem.Text;

              


                QuantityUpdate = odalBook.UpdateQ(oDaoBook);

                dt = odalBook.UpdateData(oDaoBook);

                if (QuantityUpdate == true && dt.Rows.Count >= 1)
                {

                    rptrBookData.DataSource = dt;
                    rptrBookData.DataBind();
                }
                else
                {
                    lblMessage2.Visible = true;
                    lblMessage2.Text = "Ops Something is wrong...";
                }


                status = oDalIssueBook.IssueBook(oDaoIssueBook);


                if(status==true)
                {
                    lblMessage2.Visible = true;
                    lblMessage2.Text = "This book is issued to " + ddlStudent.SelectedItem.Text + " successfully";

                }
            }
            else
            {
                lblMessage2.Visible = true;
                lblMessage2.Text = "Please Select the Branch And Student Name First..";
            }
        }

        protected void ddlPublication_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            DaoBook oDaoBook = new DaoBook();
            DalBook oDalBook = new DalBook();

            oDaoBook.Publication=ddlPublication.SelectedItem.Text;


            dt = oDalBook.ShowBook(oDaoBook);


            if(dt.Rows.Count>=1)
            {
                lblMessage.Visible = false;
                ddlBook.DataSource = dt;
                ddlBook.DataTextField ="BookName";
                ddlBook.DataValueField ="Id";
                ddlBook.DataBind();

            }
            else
            {
                lblMessage.Visible = true;
                lblMessage.Text = "Sorry No Book is Found.";
                //ddlBook.Text = "";
                ddlBook.SelectedItem.Text = "";
            }


        }

        protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            DaoStudent oDaoStudent = new DaoStudent();
            DalStudent oDalStudent = new DalStudent();

            oDaoStudent.Branch = ddlBranch.SelectedItem.Text;

            dt = oDalStudent.ShowStudent(oDaoStudent);

            if (dt.Rows.Count >= 1)
            {
                lblMessage2.Visible = false;
                ddlStudent.DataSource = dt;
                ddlStudent.DataTextField = "StudentName";
                ddlStudent.DataValueField = "Id";
                ddlStudent.DataBind();
            }
            else
            {
                lblMessage2.Visible = true;
                lblMessage2.Text = "Sorry No Student is Found.";
                //ddlBook.Text = "";
                ddlStudent.SelectedItem.Text = "";
            }


        }
    }
}