﻿using LibraryManagementSystem.Bll;
using LibraryManagementSystem.Dao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LibraryManagementSystem
{
    public partial class Login : System.Web.UI.Page
    {
        bool ret = true;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lblmessage.Visible = false;

            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if(txtEmail.Text!="" && txtPassword.Text!="")
            {
                bool status = false;

                DaoLogin oDaoLogin = new DaoLogin();
                BllLogin oBllLogin = new BllLogin();

                oDaoLogin.Email = txtEmail.Text;
                oDaoLogin.Password = txtPassword.Text;

                status = oBllLogin.FindMailPass(oDaoLogin);

                if(status==true)
                {
                    Session["LogIn"] = txtEmail.Text;
                    Response.Redirect("~/Welcome.aspx");

                }
                else
                {
                    lblmessage.Visible = true;
                    lblmessage.Text = "Incorrect mail or password.";
                }
            }
            else
            {
                lblmessage.Visible = true;
                lblmessage.Text ="Please Enter your email and password";
            }
        }
    }
}