﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryManagementSystem.Dao
{
    public class DaoBook
    {
        public int Id { get; set; }
        public string BookName { get; set; }
        public string Details { get; set; }
        public string Author { get; set; }
        public string Publication { get; set; }
        public string Branch { get; set; }
        public int Price { get; set; }
        public int Quantity { get; set; }
        public string Image { get; set; }

    }
}