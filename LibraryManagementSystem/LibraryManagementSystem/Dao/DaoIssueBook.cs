﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryManagementSystem.Dao
{
    public class DaoIssueBook
    {

        public string BookName { get; set; }
        public string Publication { get; set; }
        public string Branch { get; set; }
        public string StudentName { get; set; }
        public string Date { get; set; }
        public string Days { get; set; }
    }
}