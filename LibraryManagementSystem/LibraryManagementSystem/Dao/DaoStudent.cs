﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryManagementSystem.Dao
{
    public class DaoStudent
    {
        public int Id { get; set; }
        public string  StudentName { get; set; }
        public string Branch { get; set; }
        public string Gender { get; set; }
        public string DateOfBirth { get; set; }
        public string Mobile { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Pincode { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Photo { get; set; }
    }
}